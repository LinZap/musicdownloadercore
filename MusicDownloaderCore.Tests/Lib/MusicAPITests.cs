﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MusicDownloaderCore.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MusicDownloaderCore.Lib.Tests
{
    [TestClass()]
    public class MusicAPITests
    {
        [TestMethod()]
        public void AgentDownloadTest()
        {
          
            var url = "https://r2---sn-u4h-un5l.googlevideo.com/videoplayback?expire=1587608246&ei=VqagXoDTK9S1igbmm6q4Bg&ip=49.159.128.216&id=o-AMlzrwSRbx5_vkOdFXsvmZmMVXtk_vj1edwOnO2l3xeA&itag=140&source=youtube&requiressl=yes&mh=ud&mm=31%2C29&mn=sn-u4h-un5l%2Csn-un57en7s&ms=au%2Crdu&mv=m&mvi=1&pl=21&initcwndbps=1123750&vprv=1&mime=audio%2Fmp4&gir=yes&clen=3067550&dur=193.097&lmt=1542104141027755&mt=1587586542&fvip=2&keepalive=yes&c=WEB&txp=5432432&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cgir%2Cclen%2Cdur%2Clmt&sig=AJpPlLswRQIgShvGMxC7a99nr4FGgelhu-nmbO0d_Fvcmw6EvkoLrUICIQCAUAWlbglItpryMEvlOu7AI0LnCUzabtPycHRTC7b21Q%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=ALrAebAwRAIgJSCwJ88yRlMkF_a_4_DSXe1Rj9fjFGFehGy3oYAU6jsCIBGXL9a2pJ5NEkaP1ZWlchSFZApFSf2BPYwhvuP8pUnV";
            /*MusicAPI api = new MusicAPI();
          var b = api.AgentDownload(url);*/


            using (WebClient client = new WebClient())
            {
                var b = client.DownloadData(url);
                Assert.IsNotNull(b);
            }
        }

    }
}