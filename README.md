# NetEase Music Downloder Core

NetEase Music Download Tool. Download resources through the NetEase official API, but you need to prepare your own China VPN.


## Demo

------

### [Zap Music Hub!!](http://music.hackzap.me:8557/)

------

## Launch

1. clone this project

```
git clone https://gitlab.com/LinZap/music-downloader.git
```

2. build and publish this website 

    * .NET Framework 4.5
    * .NET Web API 2
    * IIS Service
        * WebDAV
        * URL Rewrite


3. Configuring the `FSX/Web.config` of `<appSettings>` field

    * `Storage`: prepare the root dir path for local file storage  
    * `Root`: same as `Storage`
    * `Proxy`: prepare China VPN for download music resource

    ```xml
    <add key="Storage" value="D:\FSX_Storage" />
    <add key="Root" value="D:\FSX_Storage" />
    <add key="Proxy" value="127.0.0.1:11080" />
    ```
    > I recommend using the [SpeedIn](https://www.speedin.in/) VPN, but it may require additional fees.
    

4. Launch [NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi) ServiceĦAplease read its [README.md](https://github.com/Binaryify/NeteaseCloudMusicApi/blob/master/README.MD)

```
git clone git@github.com:Binaryify/NeteaseCloudMusicApi.git
npm install
node app.js
```

> please don't change the configurion !


## UnLicense

   Copyright 2019 Zap

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.   
   