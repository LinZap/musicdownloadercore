﻿using System.Web;
using System.Web.Optimization;

namespace MusicDownloaderCore
{
    public class BundleConfig
    {
        // 如需統合的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/umd/popper.js",
                        "~/Scripts/bootstrap.js"));


            bundles.Add(new ScriptBundle("~/bundles/player").Include(
                        "~/Scripts/APlayer.min.js",
                        "~/JS/color-thief.js"));

            bundles.Add(new ScriptBundle("~/bundles/music").Include(
                      "~/JS/music.js"));

            bundles.Add(new ScriptBundle("~/bundles/string-similarity").Include(
                      "~/Scripts/string-similarity.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/ai-player").Include(
                       "~/JS/zap_recommand.js",
                       "~/JS/ai-player-core.js",
                       "~/JS/last.fm.js",
                       "~/JS/ai-player-component.js",
                       "~/JS/ai-player.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/spinkit.min.css",
                     "~/Content/bootstrap.min.css",
                     "~/Content/bootstrap-reboot.min.css",
                     "~/Content/bootstrap-grid.min.css",
                     "~/Content/APlayer.min.css",
                     "~/CSS/Site.css"));

            bundles.Add(new StyleBundle("~/Content/ai-player").Include("~/CSS/ai-player.css"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
