﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicDownloaderCore.Models.VK
{
    public class ModelVK
    {
        public string song { get; set; }
        public string artist { get; set; }
        public string url { get; set; }
    }
}