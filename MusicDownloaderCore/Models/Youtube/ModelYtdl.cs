﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicDownloaderCore.Models.Youtube
{
    public class ModelYtdl
    {
        public string id { get; set; }
        public string url { get; set; }
        public string mimeType { get; set; }
        public string extension { get; set; }
    }
}