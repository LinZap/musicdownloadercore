﻿namespace MusicDownloaderCore.Models.MusicDownloader
{
    public class ModelMusic
    {
        public string id { get; set; }
        public string author { get; set; }
        public string authorUrl { get; set; } = "#";
        public string fileName { get; set; }
        public string fileUrl { get; set; }
        public string thumb { get; set; }
    }
}