﻿using Newtonsoft.Json;

namespace MusicDownloaderCore.Models.MusicDownloader
{
    public class ModelRegisterFile
    {
        [JsonIgnore]
        public long oid { get; set; }
        public string uuid { get; set; }
        [JsonIgnore]
        public string dist { get; set; }
        public string mimeType { get; set; }
        public string filename { get; set; }
        [JsonIgnore]
        public long mid { get; set; }
    }
}