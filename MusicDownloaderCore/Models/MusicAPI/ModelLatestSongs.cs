﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelLatestSongs
    {
        public List<ModelSong> data { get; set; }
    }
}