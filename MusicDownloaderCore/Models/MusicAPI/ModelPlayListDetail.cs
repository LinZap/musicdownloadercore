﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelPlayListDetail
    {
        public ModelPlayList playlist { get; set; }
    }
    public class ModelPlayList
    {
        public List<ModelSong2Detail> tracks { get; set; }
    }
}