﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelArtistAlbum
    {
        public ModelSongArtists artist { get; set; }
        public List<ModelAlbumDetail> hotAlbums { get; set; }
    }
}