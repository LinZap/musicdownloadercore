﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelSuggest
    {
        public ModelSuggestAllMatch result { get; set; }
    }

    public class ModelSuggestAllMatch
    {
        public List<ModelSuggestMatch> allMatch { get; set; }
    }
    public class ModelSuggestMatch
    {
        public string keyword { get; set; }
    }
}