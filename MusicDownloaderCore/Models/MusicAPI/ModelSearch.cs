﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelSearch
    {
        public ModelSearchResult result { get; set; }
        public int code { get; set; }
    }

    public class ModelSearchResult
    {
        public List<ModelSong> songs { get; set; }
        public int SongCount { get; set; }
    }
}