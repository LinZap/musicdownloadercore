﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelSongURL
    {
        public List<ModelSongURLDetail> data { get; set; }
    }
    public class ModelSongURLDetail
    {
        public int id { get; set; }
        public string url { get; set; }
        public int br { get; set; }
        public int size { get; set; }
        public string level { get; set; }
    }
}