﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelSong
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<ModelSongArtists> artists { get; set; }
        public ModelSongAlbum album { get; set; }
        public int duration { get; set; }
        public List<string> alias { get; set; }
    }

    public class ModelSongAlbum
    {
        public int id { get; set; }
        public string name { get; set; }
        public ModelSongArtists artist { get; set; }
        public long publishTime { get; set; }
        public List<string> alias { get; set; }
    }

    public class ModelSongArtists
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}