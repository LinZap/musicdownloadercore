﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelAlbum : ModelSong2
    {
        public ModelAlbumDetail album { get; set; }
    }

    public class ModelAlbumDetail
    {
        public int id { get; set; }
        public string company { get; set; }
        public string picUrl { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public List<ModelSongArtists> artists { get; set; }
        public int size { get; set; }
        public long publishTime { get; set; }
    }
}