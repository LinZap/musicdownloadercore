﻿using System.Collections.Generic;

namespace MusicDownloaderCore.Models.MusicAPI
{
    public class ModelSong2
    {
        public List<ModelSong2Detail> songs { get; set; }
    }

    public class ModelSong2Detail
    {
        public string name { get; set; }
        public string id { get; set; }
        public List<ModelSong2Artist> ar { get; set; }
        public ModelSong2Album al { get; set; }
        public List<string> alia { get; set; }
        public int dt { get; set; }
    }
    public class ModelSong2Artist
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class ModelSong2Album
    {
        public int id { get; set; }
        public string name { get; set; }
        public string picUrl { get; set; }
    }
}