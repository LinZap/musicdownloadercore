﻿var ai_player_ui = {};
var ui_player = $('.aplayer');

/**
 * 錯誤統一顯示
 * @param {any} ele 錯誤訊息
 * @returns {object} jquery element
 */
ai_player_ui.error = function (ele) {
    return $('<div>').addClass('error').append(ele);
};


/**
 * 提示統一顯示
 * @param {any} ele 提示訊息
 * @returns {object} jquery element
 */
ai_player_ui.hint = function (ele) {
    var analysis = $('<div>').addClass('analysis');
    var loader = $('<div>').addClass('loader');
    var one = $('<div>').addClass('inner').addClass('one');
    var two = $('<div>').addClass('inner').addClass('two');
    var three = $('<div>').addClass('inner').addClass('three');
    var nucleus = $('<div>').addClass('nucleus');
    loader.append(one).append(two).append(three).append(nucleus);
    var text = $('<div>').addClass('hint').append(ele);
    analysis.append(loader).append(text);
    return analysis;
};

/**
 * 一個歌手項目
 * <div class="ai-player-singer-item"><img src="" /><div>Alex Goot</div></div>
 * @param {object} options data
 * @param {string} options.name 歌手名稱
 * @param {string} options.id 歌手ID
 * @param {object} options.area 區域 (不一定有)
 * @param {string} options.area.name 區域名稱 (不一定有)
 * @param {string} options.country 國家 (不一定有)
 * @param {string} options.disambiguation 消除奇異 (不一定有)
 * @returns {object} jquery element
 */
ai_player_ui.artist = function (options) {
    var img = $('<img>').data('mbid', options.id);
    var name = $('<div>').text(options.name);
    var box = $('<div>').data('mbid', options.id)
        .addClass('ai-player-singer-item')
        .append(img).append(name);
    var area = options.area ? options.area.name : options.country || options.disambiguation || '';
    lastfm.replace_artist_img(options.name, options.id, area, img);
    ai_core.basedon = null;
    box.click(e => _get_artist_top_track(options.id, options.name, img.attr('src'), area));
    return box;
};


/**
 * 取得歌手的最佳一首歌
 * @param {string} artist_mbid 歌手 mbid
 * @param {string} artist 歌手名稱
 * @param {string} artist_img 歌手大頭照連結
 * @param {string} area 歌手區域
 */
function _get_artist_top_track(artist_mbid, artist, artist_img, area) {
    _prclog('Start', 'Get Artist ' + artist + ' Top Track!');
    ui_stop1.hide();
    ui_stop2.show();
    ui_btn_gostep2.show();
    ui_player_music.html(ai_player_ui.hint('分析中...'));

    lastfm.top_tracks(artist_mbid, artist).then(json => {

        // 無法正常取得這首歌，重新分析
        if (!json.result) {
            _prclog('Error', 'Can Not Get Artist ' + artist + ' Top Track! Start To Fallback');
            // 重新取的一位歌手然後遞迴執行這個程式
            return lastfm.similarity_artist(artist).then(json => {
                // 連相似歌手都沒有，只能請使用者重選
                if (!json.result || json.data.length < 1) return ui_player_music.html(ai_player_ui.error_track());
                let top_artist = shuffle(json.data)[0];
                let artist = top_artist.name;
                return _get_artist_top_track('', artist, '', '');
            });
        }
        // 將指定的 Track 加入播放器並驗證音樂資源是否有效
        _prclog('Success', 'get top of artist tracks success', json);
        var options = {
            name: artist,
            artist: {
                mbid: artist_mbid
            }
        };
        PlayTheTrack(json, options);
    });
}


/**
 * 一首歌詳細項目
 * <div class="ai-player-music-detail">
 *  <img src="http://localhost:8013/cover.jpg" />
 *  <div class="ai-player-track-detail">
 *      <div class="ai-player-track-name">歌名</div>
 *      <div class="ai-player-artist-detail">
 *          <img src="http://localhost:8013/cover.jpg" />
 *          <div class="ai-player-artist-name">歌手</div>
 *          <div class="ai-player-album-name">專輯名稱</div>
 *      </div>
 *      <button class="ghost-btn">不喜歡</button>
 *  </div>
 * </div>
 * @param {object} options data
 * @param {string} options.artist_img artist img
 * @param {string} options.track_img track img
 * @param {string} options.track track name
 * @param {string} options.album album name
 * @param {string} options.artist artist name
 * @param {string} options.track_mbid  track ID
 * @param {string} options.artist_mbid  artist ID
 * @param {string} options.area  area name
 * @returns {object} jquery element
 */
ai_player_ui.track = function (options) {
    var btn_goback = $('<button>').addClass('ghost-btn').attr('title', '返回，添加更多不同類型').text('返回搜尋');
    var btn_dislike = $('<button>').addClass('ghost-btn').attr('title', '不喜歡，不想再聽到這首歌').text('不喜歡');
    var btn_like = $('<button>').addClass('ghost-btn').attr('title', '保留這首歌在播放清單裡，並跳到下一首新歌').text('喜歡，跳下一首');
    var btn_save_list = $('<button>').addClass('ghost-btn').attr('title', '[即將推出] 保存現有的歌單').text('保存歌單').attr('disabled', true);
    btn_goback.click(function (e) {
        // 聽到一半就跳出，算是喜歡 (因為會繼續播)，所以就直接加入   
        ai_core.like_list[ai_core.current.track_mbid] = Object.assign({}, ai_core.current);
        render_recommand();
        ui_player_main.hide();
        ui_stop1.show();
        ui_search_result.hide();
        ui_singer_sample.show();
        ui_input.val('');
        ui_input.focus();
    });
    btn_dislike.click(e => AddToAiCoreList(1, options));
    btn_like.click(e => AddToAiCoreList(0, options));

    var btn_group = $('<div>').addClass('ai-player-btn-group');
    btn_group.append(btn_goback).append(btn_save_list).append(btn_dislike).append(btn_like);
    var album_name = $('<div>').addClass('ai-player-album-name').text('/ 專輯：' + options.album);
    var artist_name = $('<div>').addClass('ai-player-artist-name').text('歌手：' + options.artist);
    //var artist_img = $('<img>').attr('src', options.artist_img);
    var artist_detail = $('<div>').addClass('ai-player-artist-detail');
    artist_detail.append(artist_name);
    if (options.album) artist_detail.append(album_name);
    var track_name = $('<div>').addClass('ai-player-track-name').text(options.track);
    var track_detail = $('<div>').addClass('ai-player-track-detail');
    track_detail.append(track_name).append(artist_detail).append(btn_group);
    var track_img = $('<img>').attr('src', options.track_img);
    var music_detail = $('<div>').addClass('ai-player-music-detail');
    music_detail.append(track_img).append(track_detail);
    //if (!options.artist_img) lastfm.replace_artist_img(options.artist, options.artist_mbid, options.area, artist_img);
    return music_detail;
};


/**
 * 無法分析出一首歌的畫面
 * @param {string} str message
 * @returns {object} jquery element
 */
ai_player_ui.error_track = function (str = null) {
    var error_msg = $('<div>').addClass('error').text(str || '無法分析，因為無法從現有的資料取得更多推薦，請返回歌曲列表增加更多資料...');
    var btn_dislike = $('<button>').addClass('ghost-btn').text('返回');
    var error_track = $('<div>').addClass('error-track');
    btn_dislike.click(function (e) {

        render_recommand();
        ui_player_main.hide();
        ui_stop1.show();
        ui_search_result.hide();
        ui_singer_sample.show();
        ui_input.val('');
        ui_input.focus();

        // 隱藏所有區塊，顯示第一步驟
        ui_player_main.hide();
        ui_stop1.show();

    });
    error_track.append(error_msg).append(btn_dislike);
    return error_track;
};


/**
 * 增加到 AI Core 的列表
 * @param {number} type 類型 0 為喜歡 , 1 為不喜歡
 * @param {object} track 為 option 物件
 * @returns {object} Promise
 */
function AddToAiCoreList(type = 0, track) {
    // 先暫停播放
    ap.pause();
    // 將這首歌放置到喜歡列表或討厭列表
    var options = Object.assign({}, track);
    var mbid = options.track_mbid;
    var resource = options.resource;
    if (type === 0) {
        ai_core.like_list[mbid] = options;
        _prclog('Start', 'AddToAiCoreList ai_core.like_list', { mbid, options });
        //console.log('AddToAiCoreList ai_core.like_list', mbid, options);
    }
    else {
        ai_core.hate_list[mbid] = options;
        delete ai_core.like_list[mbid];
        // 將該歌曲從清單中移除
        const actual_index = ap.list.audios.findIndex(e => e.custom.resource === resource);
        if (actual_index >= 0) ap.list.remove(actual_index);
        _prclog('Start', 'AddToAiCoreList ai_core.hate_list', { mbid, options });
        //console.log('AddToAiCoreList ai_core.hate_list', mbid, options);
    }

    // 預設為現有的歌
    ai_core.basedon = Object.assign({}, track);

    // 優先從喜歡列表中隨機抽取一首歌，如果沒有的話，只能從現在的歌去選
    var mbids = shuffle(Object.keys(ai_core.like_list));
    if (mbids[0]) {
        mbid = mbids[0];
        ai_core.basedon = Object.assign({}, ai_core.like_list[mbids[0]].custom);
    }

    ui_player_music.html(ai_player_ui.hint('分析中...'));

    var nombid = ai_core.like_list[mbid] ? ai_core.like_list[mbid].nombid : true;

    _prclog('Process', 'Trigger Start to find a similarity track by ' + options.artist + ' - ' + options.track + ' ' + (nombid ? 'use mbid' : 'use string'));

    return (nombid ? lastfm.similarity_track_string(options.artist, options.track, mbid) : lastfm.similarity_track(mbid))
        .then(json => {
            // 表示 lastfm 沒有註冊這首歌的相似歌曲，改回去找歌手最佳歌曲
            if (!json.result) {
                //console.log('can not get similarity_track', json);
                var track = ap.list.audios.find(d => d.custom.track_mbid === mbid) || ai_core.current;
                // 音樂撥放器裡面找不到，現在播放的紀錄也沒有，就只能重來
                if (!track) {
                    _prclog('Fail', 'Can not find any on similar track from player or ai.list', { track, ai_core_current: ai_core.current });
                    // 預設為現有的歌 (已經是)
                    var _fav_track = ai_core.current;
                    // 從喜歡列表裡面抽取其中一首歌 ID
                    var _track_mbid = shuffle(Object.keys(ai_core.like_list))[0];
                    // 如果喜歡列表裡面有歌，以那首歌為主
                    if (_track_mbid) _fav_track = ai_core.like_list[_track_mbid].custom;
                    // 如果有選到歌，就開始重新
                    if (_fav_track) {
                        _prclog('Fail', 'So Start To Fallback to Find ' + _fav_track.artist + ' Similar Artist');
                        ai_core.basedon = Object.assign({}, _fav_track);
                        return _get_artist_top_track('', _fav_track.artist, '', '');
                    }
                    return ui_player_music.html(ai_player_ui.error_track());
                }

                _prclog('Process', 'get other artist top track', { artist_mbid: track.artist_mbid, artist: track.artist, artist_img: track.artist_img, area: track.area });
                return _get_artist_top_track(track.artist_mbid, track.artist, track.artist_img, track.area);
            }


            //  將指定的 Track 加入播放器並驗證音樂資源是否有效
            _prclog('Success', 'get top of similarity tracks success', json);
            PlayTheTrack(json, options);
        });
}


/**
 * 將指定的 Track 加入播放器並驗證音樂資源是否有效
 * @param {object} json track  response data
 * @param {object} options based on current track data
 * @param {string} options.name artist name
 * @param {object} options.artist artist object
 * @param {string} options.artist.mbid artist mbid
 * @return {object} Promise 接續物件
 */
function PlayTheTrack(json, options) {
    _prclog('Start', 'PlayTheTrack', { json, options, baseon: ai_core.basedon });
    var data = json.data;
    var result = {
        artist_img: null,
        track_img: data.album ? data.album.image.length ? data.album.image[2]['#text'] : '' : '',
        track: data.name,
        album: data.album ? data.album.title : '',
        artist: data.artist ? data.artist.name : options.name,
        track_mbid: data.mbid,
        artist_mbid: options.artist.mbid,
        resource: data.resource,
        area: null,
        nombid: data.nombid,
        originTrack: ai_core.basedon ? Object.assign({}, ai_core.basedon) : ai_core.basedon
    };

    // 已經被加入的音樂，不重複加入
    if (!isMusicAdded(result.track_mbid)) {

        // 開始預載
        show_loading(true, '⏳ 正在分析音樂資源是否可用...');
        _apilog(result.resource);

        // 如果連結中沒有包含內部下載的網址，表示來源已經是 youtube，則需要重取 URL 以保證連結未過期
        if (result.resource.indexOf('/music/download') < 0)
            return use_vk_resource();
            //return use_youtube_resource();

        // 連結是內部下載
        var xhr = $.ajax({ url: result.resource, headers: { Range: 'bytes=0-1' } })
            .then((a, b, d) => {
                // 內部連結無效
                if (d.status !== 200 && d.status !== 206)
                    return use_vk_resource();
                    //return use_youtube_resource();

                // 確保音樂圖片有值
                result.track_img = result.track_img || result.artist_img || '/favicon.png';
                // 註冊要播放的檔案
                ai_core.current = result;
                // 印出歌曲詳細與資源
                ui_player_music.html(ai_player_ui.track(result));
                // 顯示撥放器，並開始播放
                ui_player.show();
                ap.pause();
                AddMusic({
                    fileName: result.track,
                    author: result.artist,
                    fileUrl: result.resource,
                    thumb: result.track_img,
                    custom: result
                });
                // 播放最後一首歌
                PlayerPlay();
            })
            .catch(e => {
                // 資源無效
                //return use_youtube_resource();
                return use_vk_resource();
            });
        return xhr;
    }

    // 使用 youtube 取代原本資源
    function use_youtube_resource() {
        lastfm.youtube(`${result.artist} ${result.track}`)
            .then(json => {

                if (!json.result) {
                    // 加入討厭清單
                    AddToAiCoreList(1, result);
                    // 關閉預載
                    return show_loading(false);
                }

                result.track_img = result.track_img || result.artist_img || '/favicon.png';
                // 註冊要播放的檔案
                ai_core.current = result;
                // 印出歌曲詳細與資源
                ui_player_music.html(ai_player_ui.track(result));
                // 顯示撥放器，並開始播放
                ui_player.show();

                // 直上 youtube 
                ai_core.current.resource = json.data;
                result.resource = json.data;
                ap.pause();
                AddMusic({
                    fileName: result.track + ' (YouTube ver.)',
                    author: result.artist,
                    fileUrl: json.data,
                    thumb: result.track_img,
                    custom: result
                });
                // 播放最後一首歌
                PlayerPlay();

            })
            .catch(e => {
                // 加入討厭清單
                AddToAiCoreList(1, result);
                // 關閉預載
                show_loading(false);
            });
    }

    // 使用更高速的 VK 取代原本資源
    function use_vk_resource() {
        lastfm.vk(`${result.artist} ${result.track}`)
            .then(json => {

                if (!json.result) {
                    // 加入討厭清單
                    AddToAiCoreList(1, result);
                    // 關閉預載
                    return show_loading(false);
                }

                result.track_img = result.track_img || result.artist_img || '/favicon.png';
                // 註冊要播放的檔案
                ai_core.current = result;
                // 印出歌曲詳細與資源
                ui_player_music.html(ai_player_ui.track(result));
                // 顯示撥放器，並開始播放
                ui_player.show();

                // 直上 vk
                ai_core.current.resource = json.url;
                result.resource = json.url;
                ap.pause();
                AddMusic({
                    fileName: result.track + ' (VK ver.)',
                    author: result.artist,
                    fileUrl: json.url,
                    thumb: result.track_img,
                    custom: result
                });
                // 播放最後一首歌
                PlayerPlay();

            })
            .catch(e => {
                // 加入討厭清單
                AddToAiCoreList(1, result);
                // 關閉預載
                show_loading(false);
            });
    }
}



/**
 * 是否已經存在播放清單
 * @param {string} mbid 音樂 ID
 * @returns {boolean} 是否已經存在
 */
function isMusicAdded(mbid) {
    for (var i = 0; i < ap.list.audios.length; i++) {
        if (mbid === ap.list.audios[i].custom.track_mbid) return true;
    }
    return false;
}
