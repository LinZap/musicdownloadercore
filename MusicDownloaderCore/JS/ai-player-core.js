﻿var ai_core = {};

/* 
 * current data
 * @param {object} current data
 * @param {string} current.artist_img artist img
 * @param {string} current.track_img track img
 * @param {string} current.teack track name
 * @param {string} current.album album name
 * @param {string} current.artist artist name
 * @param {string} current.mbid  track ID
 * */
ai_core.current = {};

/**目前撥放器索引位置 */
ai_core.index = 0;
ai_core.hate_list = {};
ai_core.like_list = {};


// 單獨判斷是否可以成為 toptrack 如果不行將會回傳 reuslt false
function _findTopTrack(track) {
    // _prclog('Process', '(_findTopTrack) is can be top track? ', track);
    return new Promise((resolve) => {
        var mbid = track.mbid;
        var artist_name = track.artist.name;
        var track_name = track.name;

        if (!mbid || mbid.length < 1) {
            //_prclog('Process', '(_findTopTrack) track ' + track_name + ' mbid is null, so get mbid');
            lastfm.get_track_mbid(artist_name, track_name).then(json => {
                if (!json.result) {
                    //_prclog('Process', '(_findTopTrack) can not get ' + track_name + ' mbid');
                    return resolve({ result: false });
                }
                else {
                    mbid = json.data.id;
                    track.mbid = json.data.id;
                    track.nombid = true;
                    var is_in_hate = !!ai_core.hate_list[mbid];
                    var is_in_like = !!ai_core.like_list[mbid];
                    var is_in_list = isMusicAdded(mbid);
                    if (is_in_hate || is_in_like || is_in_list) {
                        //var errmsg = '(_findTopTrack) ' + track_name + ' (' + mbid + ') is exists in ';
                        //if (is_in_hate) errmsg += 'hate list';
                        //else if (is_in_like) errmsg += 'like list';
                        //else if (is_in_list) errmsg += 'music list';
                        //_prclog('Process', errmsg);
                        return resolve({ result: false });
                    }
                    return resolve({ result: true, data: track });
                }
            });
        }
        else if (!!ai_core.hate_list[mbid] || !!ai_core.like_list[mbid] || isMusicAdded(mbid)) {

            //var errmsg = '(_findTopTrack) ' + track_name + ' (' + mbid + ') is exists in ';
            //if (!!ai_core.hate_list[mbid]) errmsg += 'hate list';
            //else if (!!ai_core.like_list[mbid]) errmsg += 'like list';
            //else if (isMusicAdded(mbid)) errmsg += 'music list';
            //_prclog('Process', errmsg);
            return resolve({ result: false });
        }
        else {
            //_prclog('Process', '(_findTopTrack) finded!', track);
            resolve({ result: true, data: track });
        }
    });
}


// 找出最好的一首歌 (注意這裡是需要用 then 去接)
ai_core.findTopTrack = function (list) {
    // 隨機排序
    list = shuffle(list);
    _prclog('Process', '(ai_core.findTopTrack) find one of track from list', list);
    // console.log('find one of top track', list);
    // 循環取得 mbid
    var p = _findTopTrack(list[0]);
    var cand_list = [];
    for (var i = 1; i < list.length; i++) {
        p = (function (track) {
            return p.then(function (json) {
                /* 照順序跑，第一個 result = true 的就一路傳到最後
                return json.result ?
                    new Promise(resolve => resolve(json))
                    : _findTopTrack(track);*/
                // 全部都跑
                if (json.result) cand_list.push(json.data);
                return _findTopTrack(track);
            });
        })(list[i]);
    }

    p = p.then((json) => {
        if (json.result) cand_list.push(json.data);
        _prclog('Process', '(ai_core.findTopTrack) fined: ' + cand_list.length + ' track ', cand_list);
        return { data: cand_list, result: true };
    });

    // 也可能會全都取不到
    return p;
};



// 評估是否為同一首歌
ai_core.similarity_threshold = 0.5;
ai_core.analysis_similarity = function (actual_artist, actual_track, target_artist, target_track) {
    actual_artist = actual_artist.toLowerCase().trim();
    actual_track = actual_track.toLowerCase().trim();
    target_artist = target_artist.toLowerCase().trim();
    target_track = target_track.toLowerCase().trim();
    var sim_artist = stringSimilarity.compareTwoStrings(actual_artist, target_artist);
    var sim_track = stringSimilarity.compareTwoStrings(actual_track, target_track);
    var score = (sim_artist + sim_track) / 2;
    //console.log('analysis_similarity', { actual_artist, target_artist, actual_track, target_track, score });
    return score;
};



/**
 * 是否已經存在播放清單 (0.7 相似度就算)
 * @param {string} artist 歌手名稱
 * @returns {boolean} 是否已經存在
 */
function isArtistAdded(artist) {
    var actual_artist = artist.toLowerCase().trim();
    for (var i = 0; i < ap.list.audios.length; i++) {
        var target_artist = ap.list.audios[i].custom.artist.toLowerCase().trim();
        if (stringSimilarity.compareTwoStrings(actual_artist, target_artist) >= 0.7) return true;
    }
    return false;
}
