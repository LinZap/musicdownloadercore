﻿document.title = 'AI. Player';
var ui_player = $('.aplayer');
var ui_input = $('#input-search-artist');
var ui_singer_sample = $('#ai-player-singer-sample');
var ui_search_form = $('#ai-player-form-search-singer');
var ui_search_result = $('#ai-player-search-result');
var ui_search_result_list = $('#ai-player-search-result-list');
var ui_stop1 = $('#ai-player-step1');
var ui_stop2 = $('#ai-player-step2');
var ui_player_main = $('.ai-player-main');
var ui_player_music = $('#ai-player-music');
var ui_loading = $('#loading-cover');
var ui_time_back = $('#time-back');
var ui_recommand = $('#ai-player-recommand-list');
var ui_btn_gostep2 = $('#btn_gostep2');
var ui_runhorse = $('#runhorse');
var ui_loading_text = $('#loading-text');
var runhorse = [
    '不用再自己找歌了！讓 AI. Player 來發掘你從不知道的好歌！',
    '不用擔心！選定一首歌之後，系統將會無限循環的挖掘新歌給你！',
    '這是 <a href="https://github.com/LinZap/" target="_blank">ZapLin</a> 全新嘔心瀝血新作品，希望大家喜歡！',
    '上班再也不用一直切螢幕找歌了，放著一直聽下去就對了！',
    '由於西洋音樂資料比較齊全，所以英文歌推薦會比較準確！',
    '有些歌因為版權問題，所以系統會直接跳過，這是正常的喔！',
    '有發現 Bug 也歡迎提交 <a href="https://gitlab.com/LinZap/musicdownloadercore/-/issues" target="_blank">Issue</a>！',
    //'4/21 這一版更新了許多原先無法聽的熱門歌曲與歌手',
    //'4/22 這一版更新了首頁推薦的歌手清單',
    // '4/23 🔥 目前這個版本開啟了實驗功能(未來可能移除) 🔥',
    '4/25 大幅優化了各種操作的 BUG，修復大量使用上會出現的問題！',
    '4/26 大幅提升下載效能並提供 VK Music 作為第二順位的音樂來源！'
];


/**
 * 開關 loading 畫面
 * @param {any} open 開啟或關閉
 */
var time_back_interval = null;
var wait_sec = 25;
function show_loading(open = true, msg = '⏳ 音樂資源下載中...') {
    if (open) {
        wait_sec = 25;
        ui_time_back.text(wait_sec);
        clearInterval(time_back_interval);
        time_back_interval = setInterval(function () {
            if (wait_sec > 0) wait_sec--;
            ui_time_back.text(wait_sec);
        }, 1000);
        ui_loading_text.text(msg);
        ui_loading.show();
    }
    else {
        clearInterval(time_back_interval);
        ui_loading.hide();
    }
}

// 隱藏所有 runhorse 
ui_runhorse.hide();

// 隱藏返回猜你喜歡
ui_btn_gostep2.hide();

// 隱藏載入中
show_loading(false);

$('body').css('background-color', 'transparent');

// 隱藏撥放器
ui_player.addClass('aplayer-border').hide();
// 隱藏所有區塊
ui_player_main.hide();
// 附加撥放器CSS
$('.aplayer-pic').css('background-color', 'transparent');

// 隱藏搜尋結果
ui_search_result.hide();
// 顯示第一步驟區塊
ui_stop1.show();

// 印出推薦
render_recommand();

// 印出推薦
function render_recommand() {
    singer_list = shuffle(singer_list);
    var zap_recommands = singer_list.slice(0, 9);
    ui_recommand.html('');
    for (var i = 0; i < zap_recommands.length; i++) {
        var artist = zap_recommands[i];
        var ui_arisit = ai_player_ui.artist(artist);
        ui_recommand.append(ui_arisit);
    }
}



// 送出搜尋
ui_search_form.submit(function (e) {
    e.preventDefault();
    var artist = ui_input.val().trim();
    if (artist.length < 1) return;

    // 顯示搜尋，隱藏推薦
    ui_singer_sample.hide();
    ui_search_result.show();
    ui_search_result_list.html('').append(ai_player_ui.hint('載入中...'));

    // 搜尋歌手
    lastfm.search_artist(artist).then(json => {
        //console.log('search_artist', json);
        // api 失敗
        if (!json.result) {
            ui_search_result_list.html('').append(ai_player_ui.error('搜尋發生錯誤'));
        } // api 成功
        else {
            ui_search_result_list.html('');
            var len = Math.min(json.data.length, 9);
            for (var i = 0; i < len; i++) {
                var artist = json.data[i];
                var ui_arisit = ai_player_ui.artist(artist);
                ui_search_result_list.append(ui_arisit);
            }
        }
    });
});


// 返回作者推薦
$('#btn-go-suggest').click(function (e) {
    // 顯示推薦，隱藏搜尋
    ui_search_result.hide();
    ui_singer_sample.show();
    ui_input.val('');
    ui_input.focus();
});

// 返回猜你喜歡
ui_btn_gostep2.click(function (e) {
    // 隱藏所有區塊
    ui_player_main.hide();
    ui_stop2.show();
});

// 附加事件(播放到結束)
// 跳下一個推薦
ap.on('ended', function () {
    _prclog('Start', 'ap.on.ended (ai.player)');
    // 取得上一首歌，因為 ended 事件竟然在 listswitch 之後，非常扯。它會自動跳下一首歌，所以要重新推算上一首歌
    if (ai_core.prevIndex > -1) {
        var track = ap.list.audios[ai_core.prevIndex].custom;
        AddToAiCoreList(0, track);
    }
});

// 切換時，連帶切換顯示與紀錄
ap.on('listswitch', (data) => {
    _prclog('Start', 'ap.on.listswitch (ai.player)', data);
    ai_core.prevIndex = ap.list.index;
    ai_core.index = data.index;
    ai_core.current = ap.list.audios[data.index].custom;
    ui_player_music.html(ai_player_ui.track(ai_core.current));
});


// 開始下載音樂
ap.on('loadstart', function () {
    _prclog('Start', 'ap.on.loadstart (ai.player)');
    show_loading();
    document.title = 'AI. Player 下載: ' + ai_core.current.artist + ' - ' + ai_core.current.track;
});

// 下載完畢
ap.on('loadeddata', function () {
    _prclog('Start', 'ap.on.loadeddata (ai.player)');
    show_loading(false);
    document.title = 'AI. Player: ' + ai_core.current.artist + ' - ' + ai_core.current.track;
    // 播放最後一首歌
    //PlayerPlay();
});







// 啟動 run horse
var runhorse_index = 0;
function ShowRunHorse() {
    if (runhorse_index >= runhorse.length) runhorse_index = 0;
    ui_runhorse.hide().html(runhorse[runhorse_index]).fadeIn(1000);
    runhorse_index++;
    setTimeout(ShowRunHorse, 6000);
}
ShowRunHorse();