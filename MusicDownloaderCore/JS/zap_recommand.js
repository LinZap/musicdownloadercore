﻿// 隨機排列陣列
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}


var singer_list = [
    {
        name: 'Maroon 5',
        id: '0ab49580-c84f-44d4-875f-d83760ea2cfe',
        area: { name: 'United States' }
    },
    {
        name: 'Against the Current',
        id: 'c40a79e9-3b9e-492e-9469-0e5b952ad3e1',
        area: { name: 'United States' }
    },
    {
        name: 'One Direction',
        id: '1a425bbd-cca4-4b2c-aeb7-71cb176c828a',
        area: { name: 'United Kingdom' }
    },
    {
        name: 'Mrs. GREEN APPLE',
        id: '9ce674b7-5180-41f7-9ac2-95dc0d8a0ed2',
        area: { name: 'Japan' }
    },
    {
        name: 'イヤホンズ',
        id: 'b9747d39-9f6e-42b7-b43e-17f6cae07e09',
        area: { name: 'Japan' }
    },
    {
        name: 'モーニング娘',
        id: '9bffb20c-dd17-4895-9fd1-4e73e888d799',
        area: { name: 'Japan' }
    },
    {
        name: 'LiSA',
        id: '85d76093-9865-4605-97fa-8c910929d366',
        area: { name: 'Japan' }
    },
    {
        name: '亜咲花',
        id: '2c690c27-b855-4d3c-9efe-a773ad9009a8',
        area: { name: 'Japan' }
    },
    {
        name: '和樂器',
        id: 'c5ae5832-cbbf-44d0-8b9c-36e67360c5b1',
        area: { name: 'Japan' }
    },
    {
        name: '渡り廊下走り隊',
        id: '14b91b15-1c04-4081-80dc-b304b9cee606',
        area: { name: 'Japan' }
    },
    {
        name: 'LADYBABY',
        id: 'c464e32e-a352-4002-ae11-809e359df3f8',
        area: { name: 'Japan' }
    },
    {
        name: 'まねきケチャ',
        id: 'e07a747d-9ec4-4012-a09a-a45d690bff6d',
        area: { name: 'Japan' }
    },
    {
        name: '綾野ましろ',
        id: '6fb7e0b2-9a79-4aaa-a1b4-a5fa265dd759',
        area: { name: 'Japan' }
    },
    {
        name: 'YUI',
        id: '80d1d7e0-fdab-4009-8375-54bd63d74c0e',
        area: { name: 'Japan' }
    },
    {
        name: 'YUI',
        id: '80d1d7e0-fdab-4009-8375-54bd63d74c0e',
        area: { name: 'Japan' }
    },
    {
        name: 'GARNiDELiA',
        id: '9bdb5e93-32e4-4dbb-9fac-b8771e1c1551',
        area: { name: 'Japan' }
    },
    {
        name: '虹のコンキスタドール',
        id: 'c1d4a924-93e5-4364-9b46-5e44b4513195',
        area: { name: 'Japan' }
    },
    {
        name: 'でんぱ組.inc',
        id: '1e9b14ce-98a4-402e-b0a2-54a606f4093d',
        area: { name: 'Japan' }
    },
    {
        name: 'Buono!',
        id: 'cae0e51e-4af0-48b4-ac00-209ba1e83bcc',
        area: { name: 'Japan' }
    },
    {
        name: 'ももいろクローバーZ',
        id: '3862143a-a878-4176-88d5-a0cfae83cdf6',
        area: { name: 'Japan' }
    },
    {
        name: '平野綾',
        id: 'dfe0486a-d8f7-449a-a1ce-192803b3b537',
        area: { name: 'Japan' }
    },
    {
        name: 'i☆Ris',
        id: '11db7aec-f54a-4623-b97e-5b94cd954c9e',
        area: { name: 'Japan' }
    },
    {
        name: 'SUPER☆GiRLS',
        id: '4d49e67b-1746-48ac-9eac-9ebabc595486',
        area: { name: 'Japan' }
    },
    {
        name: '私立恵比寿中学',
        id: 'f13e929f-6836-4f02-8175-b86c31bb91ec',
        area: { name: 'Japan' }
    },
    {
        name: 'TEAM SHACHI',
        id: '7c9e1229-1b01-4072-80cd-c95d016165ac',
        area: { name: 'Japan' }
    },
    {
        name: 'ときめき♡宣伝部',
        id: 'b901c170-0045-4555-8472-8ff0d59cbefe',
        area: { name: 'Japan' }
    },
    {
        name: 'BACK-ON',
        id: '12d75989-f4c5-49c0-9785-91df3d160648',
        area: { name: 'Japan' }
    },
    {
        name: 'Reol',
        id: '2db42837-c832-3c27-b4a3-08198f75693c',
        area: { name: 'Japan' }
    },
    {
        name: '西野カナ',
        id: '04da690e-d317-428a-b231-43fc4b59ef83',
        area: { name: 'Japan' }
    },
    {
        name: 'Goose house',
        id: '46c8042e-881e-431c-adea-57c975bd0018',
        area: { name: 'Japan' }
    },
    {
        name: '氣志團',
        id: '9553a8ac-ddcb-40aa-8863-23a86869b68d',
        area: { name: 'Japan' }
    },
    {
        name: 'm.o.v.e',
        id: 'b58a2815-9c15-4fc8-b023-9bea7207e877',
        area: { name: 'Japan' }
    },
    {
        name: '島谷ひとみ',
        id: 'ee4c184e-90a5-44a6-b876-e692bb5d9a7e',
        area: { name: 'Japan' }
    },
    {
        name: 'E‐girls',
        id: '65784683-d180-44da-b385-050acf8ec4bb',
        area: { name: 'Japan' }
    },
    {
        name: 'SCANDAL',
        id: 'd4c7c69d-a1f9-4717-a409-0346fd90d517',
        area: { name: 'Japan' }
    },
    {
        name: 'miwa',
        id: 'a83e47fb-d486-4c15-9919-ed8351cc395c',
        area: { name: 'Japan' }
    },
    {
        name: '[Alexandros]',
        id: '03bef19b-535e-4b8a-a882-01fcb7324386',
        area: { name: 'Japan' }
    },
    {
        name: 'DA PUMP',
        id: '8b5d238f-783b-4266-a621-5dac8a7bd301',
        area: { name: 'Japan' }
    },
    {
        name: 'ステレオポニー',
        id: '226c278c-a092-402c-95a4-069b8df3709f',
        area: { name: 'Japan' }
    },
    {
        name: '妖精帝國',
        id: 'd02974dc-b4dc-4691-9705-33929ac731e1',
        area: { name: 'Japan' }
    },
    {
        name: 'SPYAIR',
        id: '668d4190-f273-4470-9d1a-15d4d2d589c6',
        area: { name: 'Japan' }
    },
    {
        name: 'FLOW',
        id: 'a0387b9b-236d-475f-8846-b2dea4af1112',
        area: { name: 'Japan' }
    },
    {
        name: 'sumika',
        id: 'bc731e96-0c06-4adf-b94b-ca636f48998a',
        area: { name: 'Japan' }
    }
];

