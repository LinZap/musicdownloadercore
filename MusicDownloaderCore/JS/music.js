﻿function convertUnixTimeStamp(unixtimestamp) {
    let date = new Date(unixtimestamp);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    return year + '/' + month + '/' + day;
}

var MusicCurrent;
var MusicIndex = 0;

const tmplate_err = `<div class='container text-center'>無法取得結果</div>`;
const tmplate_loading = `<div class='container text-center'>
                <div class="spinner-border" style="width: 10rem; height: 10rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                </div>`;

const ap = new APlayer({
    container: document.getElementById('aplayer'),
    listFolded: false,
    listMaxHeight: '200px',
    theme: '#e9e9e9',
    volume: 1,
    mutex: true,
    audio: [],
    loop: ISAIPLAYER ? 'none' : 'all',
    preload: ISAIPLAYER ? 'none' : 'auto'
});


const colorThief = new ColorThief();
const setTheme = (index) => {
    if (ap.list.audios[index]) {
        if (!ap.list.audios[index].color || !ap.list.audios[index].theme) {
            colorThief.getColorAsync(ap.list.audios[index].cover, function (color) {
                color = Array.isArray(color) ? color : [255, 255, 255];
                ap.list.audios[index].color = color;
                setColor(color, index);
            });
        }
        else {
            setColor(ap.list.audios[index].color, index);
        }
    }
};

const setColor = (color, index) => {
    ap.theme(`rgb(${color[0]}, ${color[1]}, ${color[2]})`, index);
    $('.aplayer-border').css('box-shadow', `0px 0px 70px 40px rgba(${color[0]}, ${color[1]}, ${color[2]}, 0.31)`)
        .css('border', `solid 0.5px rgba(${color[0]}, ${color[1]}, ${color[2]}, 0.5)`);
    $('#ai-player-step2').css('box-shadow', `0px 0px 18px 6px rgba(${color[0]}, ${color[1]}, ${color[2]}, 0.1)`);
    $('.ai-player-top>h1').css('text-shadow', `0px 0px 5px rgba(${color[0]}, ${color[1]}, ${color[2]}, 1)`);
};


setTheme(ap.list.index);
ap.on('listswitch', (data) => {
    _prclog('Start', 'ap.on.listswitch');
    MusicIndex = data.index;
    MusicCurrent = ap.list.audios[MusicIndex];
    setTheme(data.index);
});



ap.on('error', function (e) {
    var path = e.path;
    if (!Array.isArray(path)) return;
    path.forEach(audio => {
        var url = audio.currentSrc;
        _prclog('Fail', 'An error occurred: ' + url);
        var idx = ap.list.audios.findIndex(d => d.url === url);
        // 已經不能播放了，只能加入討厭清單
        if (idx > -1) {
            var track = ap.list.audios[idx].custom;
            if (track) AddToAiCoreList(1, track);
            ap.list.remove(idx);
        }
        else {
            _prclog('Fail', 'Can not find: ' + url + ' in  ap.list.audios so skip to remove');
        }
    });

    // 檢查有沒有 AI 播放器，沒有的話直接跳出
    if (!ISAIPLAYER) return;

    //ap.pause();
    document.title = 'AI. Player';
    show_loading(false);

    /*
    
    if (!MusicCurrent.url) {
        ap.list.remove(MusicIndex);
    }
    else {
        //var reg1 = new RegExp(/id=(\d+)/);
        var url1 = MusicCurrent.url;
        // double check the resource is fail
        $.get(MusicCurrent.url).catch(function (q) {
            ap.notice(q.responseText, 4000);
            let idx = ap.list.audios.findIndex(ele => {
                //var reg2 = new RegExp(/id=(\d+)/);
                //var id2 = reg2.exec(ele.url)[1];
                var url2 = ele.url;
                console.log('on.error: will remove list ,so find Index', url1, url2, url1 === url2);
                return url1 === url2; 
            });
            if (idx >= 0) {
                console.log('on.error: remove list, ', idx, ap.list.audios[idx]);
                ap.list.remove(idx);
            }
        });
    }*/
});



$('#search-form').submit(function (event) {

    PageTo('search');

    event.preventDefault();

    var str = $('#search').val().trim();

    var result = $("#result");

    result.html(tmplate_loading);

    $.getJSON("/music/search?", { keywords: str })
        .done(function (data) {
            var row = "";
            $.each(data.result.songs, function (i, item) {
                row += renderRow(item);
            });
            result.html(renderTable(row));
            $('[data-toggle="tooltip"]').tooltip();
        })
        .fail(function () {
            result.html(tmplate_err);
        });

});

$('.btn_loadplaylist').click(function (e) {
    let btn = $(this);
    let id = btn.data('id');
    btn.text('Loading...').attr('disabled', 'disabled');
    $.getJSON("/music/playlist?", { id: id })
        .done(function (data) {
            $.each(data, function (i, item) {
                AddMusic(item);
            });
            btn.hide();
            alert('載入成功!');
            PlayerPlay();
        })
        .fail(function () {
            btn.text('重新載入').removeAttr('disabled');
            alert('載入失敗!');
        });

});

function PlayTheMusic(id) {
    let t = $(this).text("loading");
    t.unbind('click');
    $.getJSON("/music/detail?", { id: id })
        .done(function (data) {
            AddMusic(data);
            t.text("Added");
            PlayerPlay();
        })
        .fail(function () {
            alert("無法取得音樂細節");
            t.text("失效");
        });
}

/**
 * 新增一首歌到撥放器
 * @param {object} data data
 * @param {string} data.fileName 歌曲名稱
 * @param {string} data.author 歌手
 * @param {string} data.fileUrl 歌曲檔案
 * @param {string} data.thumb 專輯封面
 * @param {object} data.custom custom data
 */
function AddMusic(data) {

    _prclog('Start', 'AddMusic', data);

    var idx = ap.list.audios.findIndex(d => d.url === data.fileUrl);
    if (idx < 0) {
        ap.list.add([{
            name: data.fileName,
            artist: data.author,
            url: data.fileUrl,
            cover: data.thumb,
            custom: data.custom
        }]);
        $('.aplayer-icon-forward,.aplayer-icon-back').css('display', 'inline-block');
    }
    else {
        _prclog('Fail', 'track is exists , player switch (jump) to : ' + idx, { fileUrl: data.fileUrl });
        ap.list.switch(idx);
        ap.play();
    }
}


/**
 * 播放最後被加入的音樂
 * */
function PlayerPlay() {
    ap.list.switch(ap.list.audios.length - 1);
    ap.play();
}


function LimitPlay(e) {
    var ele = $(e);
    AddMusic({
        fileName: ele.data('filename'),
        author: ele.data('artist'),
        fileUrl: ele.data('url'),
        thumb: ele.data('cover')
    });
    ele.hide();
    PlayerPlay();
}

function renderTable(content) {
    return `
    <table class="table table-hover table-striped container">
        <thead>
            <tr>
                <th scope="row" class="nowarp">下載</th>
                <th scope="row" class="nowarp">播放</th>
                <th scope="row">歌曲名稱</th>
                <th scope="row" class="nowarp">長度</th>
                <th scope="row">歌手</th>
                <th scope="row">專輯</th>     
                <!--<th scope="row">備註</th>-->         
            </tr>
        </thead>
        <tbody>${content}</tbody>
    </table>`;
}

function renderRow(data) {
    let artist = renderProfile(data.artists || data.ar);
    let album = renderProfile([data.album || data.al]);
    let note = renderNote(data.alias || data.alia);
    let lng = millisToMinutesAndSeconds(data.duration || data.dt);
    let download_link = "/music/download?id=" + data.id + "&filename=" + artist.name + " - " + data.name;

    return `
    <tr data-id="${data.id}" data-song="${data.name}" data-singer="${artist.name}" data-album="${album.name}" data-note="${note}" data-lng="${lng}" data-toggle="tooltip" data-placement="top" title="${note}">
        <th scope="row" class="nowarp"><a target="_blank" href="${download_link}">下載</a></th>
        <td onclick="PlayTheMusic.bind(this)(${data.id})">播放</td>
        <td>${data.name}</td>
        <td  class="nowarp">${lng}</td>
        <td class="link" onclick="linkto('artist',${artist.id})">${artist.name}</td>
        <td class="link" onclick="linkto('album',${album.id})">${album.name}</td>
        <!--<td >${note}</td>-->
    </tr>`;
}



function renderProfile(data) {
    if (data.length < 1) return ``;
    if (!data[0]) return ``;
    return {
        name: data[0].name,
        id: data[0].id
    };
}

function renderNote(data) {
    if (!data) return "";
    let str = data.join(" ; ");
    return str.length > 20 ? str.slice(0, 20) + "..." : str;
}


function millisToMinutesAndSeconds(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = (millis % 60000 / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}





////////////////////virtual page function///////////////////
var btn_back = $('#btn_back'),
    btn_home = $('#btn_home');

PageTo('index');

btn_back.click(function (e) {
    PageTo('index');
});

function showBack(control) {
    if (control) {
        btn_home.hide();
        btn_back.show();
    }
    else {
        btn_back.hide();
        btn_home.show();
    }
}

function PageTo(name = 'index') {
    $('.vpage').hide();
    $('.vpage-' + name).show();
    if (name === 'index') showBack(false);
    else showBack(true);
}

////////////////////////////////////////////////////////////////////////

var MUSIC_TYPE = {
    jp: 8,
    ko: 16,
    ch: 7,
    en: 96
};

// index page
for (var k in MUSIC_TYPE) getLatestSongs(k);

// name: jp,ko,ch,en
function getLatestSongs(name) {

    var result = $("#nav-" + name);
    result.html(tmplate_loading);
    $.getJSON("/music/latestsongs?", { type: MUSIC_TYPE[name] })
        .done(function (data) {
            var row = "";
            $.each(data, function (i, item) {
                row += renderRow(item);
            });
            result.html(renderTable(row));
            $('[data-toggle="tooltip"]').tooltip();
        })
        .fail(function () {
            result.html(tmplate_err);
        });
}


function linkto(type, id) {

    PageTo(type);
    if (type === 'artist') renderArtistPage(id);
    else if (type === 'album') renderAlbumPage(id);
}

function renderArtistPage(id) {
    let page = $('#artistpage');
    page.html(tmplate_loading);
    $.getJSON("/music/artist?", { id: id })
        .done(function (data) {
            page.html(`
                <div class="artist-header">
                    <img src="${data.artist.picUrl}"/>
                    <h3>${data.artist.name}</h3>
                </div>
                <h5 class="artist-albumtitle">專輯列表</h5>
                <div class="artist-album">${renderAlbumList(data.hotAlbums)}</div>
            `);
        })
        .fail(function () {
            page.html(tmplate_err);
        });
}


function renderAlbumList(hotAlbums) {
    if (!Array.isArray(hotAlbums)) return "沒有任何專輯";
    return hotAlbums.map(album => `
        <div class="album-item" onclick="linkto('album',${album.id})">
            <img src="${album.picUrl}?param=150y150">
            <div class="album-info">
                <div class="album-name">${album.name}</div>
                <div class="album-date">${convertUnixTimeStamp(album.publishTime)}</div>
                <div class="album-size">歌曲數量：${album.size}</div>
                <div class="album-comp">${album.company}</div>
            </div>
        </div>
    `).join('');
}



function renderAlbumPage(id) {
    let page = $('#albumpage');
    page.html(tmplate_loading);
    $.getJSON("/music/album?", { id: id })
        .done(function (data) {
            let album = data.album;
            page.html(`
                <div class="album-item album-header">
                    <img src="${album.picUrl}?param=150y150">
                    <div class="album-info">
                        <div class="album-name">${album.name}</div>
                        <div class="album-date">${convertUnixTimeStamp(album.publishTime)}</div>
                        <div class="album-size">歌曲數量：${album.size}</div>
                        <div class="album-comp">${album.company}</div>
                    </div>
                </div>
                <h5 class="artist-albumtitle">歌曲列表</h5>
                <div class="album-songs">${renderAlbumSongsTable(data.songs)}</div>
            `);
            $('[data-toggle="tooltip"]').tooltip();
        })
        .fail(function () {
            page.html(tmplate_err);
        });
}

function renderAlbumSongsTable(songs) {
    let row = "";
    $.each(songs, function (i, item) {
        row += renderRow(item);

    });
    return renderTable(row);
}



function _prclog(status, str, data) {
    if (!ISDEBUG) return;
    let color = '#888888';
    switch (status) {
        case 'Success': color = '#3c9c27'; break;
        case 'Fail': color = '#b0321c'; break;
        case 'Process': color = '#888888'; break;
        case 'Start': color = '#ebc015'; break;
    }
    //console.log('%c [API] ' + url, 'font-weight:bold; color: #48a');
    console.log('%c [' + status + '] ' + str, 'color:' + color + '; font-weight:bold');
    if (data) console.log(data);
}

function _apilog(url) {
    if (!ISDEBUG) return;
    console.log('%c [API] ' + url, 'font-weight:bold; color: #48a');
}