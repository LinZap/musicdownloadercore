﻿var musicbrainz_url = "https://musicbrainz.org/ws/2";
var lastfm_url = "http://ws.audioscrobbler.com/2.0";
var fanart_url = "https://webservice.fanart.tv/v3";
var apikey = "cbbc7545b77c2b31af645278c9f0a9c7";
var prjkey = "d244d22a0757e37db599d5bf3e2e35b0";

var lastfm = {};

// 搜尋歌手
lastfm.search_artist = function (q) {
	_apilog(`${musicbrainz_url}/artist?query=${q}&fmt=json&limit=9`);
	return $.getJSON(`${musicbrainz_url}/artist?query=${q}&fmt=json&limit=9`)
		.then(json => ({ result: true, data: json.artists }))
		.catch(e => ({ result: false, error: e, data: [] }));
};

// 歌手圖片
lastfm.replace_artist_img = function (name, mbid, area, img) {
	_apilog(`${fanart_url}/music/${mbid}&?api_key=${prjkey}&format=json`);
	$.getJSON(`${fanart_url}/music/${mbid}&?api_key=${prjkey}&format=json`)
		.then(json => {
			if (json.artistthumb && json.artistthumb.length) {
				img.attr('src', json.artistthumb[0].url);
			}
			else {
				img.attr('src', `https://tse2.mm.bing.net/th?q=${name}%20singer&w=100&h=100&c=7&rs=1`);
				_prclog('Fail', 'can not get artist img from fanart', { json, name, mbid, area, img });
				//console.log('replace_artist_img can not get artistthumb', { json, name, mbid, area, img });
			}
		})
		.catch(() => {
			_prclog('Fail', 'can not get artist img from fanart', { name, mbid, area, img });
			//console.log('replace_artist_img.fail', { name, mbid, area, img });
			img.attr('src', `https://tse2.mm.bing.net/th?q=${name}%20${area}&w=100&h=100&c=7&rs=1`);
		});
};

// 最佳歌曲
lastfm.top_tracks = function (mbid, artist) {
	_apilog(`${lastfm_url}?method=artist.gettoptracks&mbid=${mbid}&limit=10&api_key=${apikey}&format=json`);
	return $.getJSON(`${lastfm_url}?method=artist.gettoptracks&mbid=${mbid}&limit=10&api_key=${apikey}&format=json`)

		.then(json => {
			_prclog('Process', 'get toptracks', json);
			return !json.toptracks || json.toptracks.track.length < 1 ?
				{ result: true, data: [], error: 'can not get toptracks (mbid has not regist at lastfm)' }
				: { result: true, data: json.toptracks.track };
		})
		.then(json => {
			//console.log('top_tracks', json);
			// 連 top_track 無法正常取得 (API 死亡)
			if (!json) {
				_prclog('Fail', 'can not get toptracks', json);
				return json;
			}
			// 可以取得 top_track 但是沒有結果 (lastfm 沒有註冊 mbid) 改由字串查詢
			if (json.data.length < 1) {
				_prclog('Fail', 'can not get any top track, so go back to search artist top track', json);
				return lastfm.top_tracks_string(artist);
			}
			return json;
		})

		.then(json => ai_core.findTopTrack(json.data)
			.then(json => {

				_prclog('Process', 'success get list of top track list and ready to vaild their resource', json);

				// 如果沒有任何可用資源直接結束
				if (!json.data || json.data.length < 1) {
					_prclog('Fail', 'can not get any one track data', json);
					return { result: false, error: 'can not get top track', data: {} };
				}

				// 開始迴圈找歌
				var i = 0, top_track = json.data[i];
				var p = top_track.nombid ? lastfm.tracks_string(top_track.artist.name, top_track.name, top_track.mbid) :
					lastfm.tracks(top_track.mbid);
				// 依照順序如果第一首歌都可使用，將會被一路一直傳到最後，中間的不會再執行
				for (i = 1; i < json.data.length; i++) {
					p = (top_track => p.then(json => {
						return json.result ?
							json :
							top_track.nombid ?
								lastfm.tracks_string(top_track.artist.name, top_track.name, top_track.mbid) :
								lastfm.tracks(top_track.mbid);
					}))(json.data[i]);
				}
				return p;
			})
		)
		.catch(e => ({ result: false, error: e, data: [] }));
};

// 最佳歌曲 (字串查詢)
lastfm.top_tracks_string = function (artist) {
	_apilog(`${lastfm_url}?method=artist.gettoptracks&artist=${artist}&limit=10&autocorrect=1&api_key=${apikey}&format=json`);
	return $.getJSON(`${lastfm_url}?method=artist.gettoptracks&artist=${artist}&limit=10&autocorrect=1&api_key=${apikey}&format=json`)
		.then(json => {
			if (!json.toptracks || json.toptracks.track.length < 1) {
				_prclog('Fail', 'can not get top track by artist', json);
				return { result: false, data: [], error: '[use string] no any top_track' };
			}
			else {
				_prclog('Success', 'git top track by artist', json);
				return { result: true, data: json.toptracks.track };
			}
		})
		.catch(e => ({ result: false, error: e, data: [] }));
};



// 歌曲細節
lastfm.tracks = function (mbid) {
	_apilog(`${lastfm_url}?method=track.getInfo&mbid=${mbid}&api_key=${apikey}&format=json`);
	return $.getJSON(`${lastfm_url}?method=track.getInfo&mbid=${mbid}&api_key=${apikey}&format=json`)
		.then(json => {
			if (json.track) {
				_prclog('Success', 'get track detail success', json);
				return { result: true, data: json.track };
			}
			else {
				_prclog('Fail', 'can not get track detail', json);
				return { result: false, error: 'can not get track detail', data: {} };
			}
		})
		.then(json => {
			if (!json.result) return json;
			var track = json.data.name || '';
			var artist = json.data.artist ? json.data.artist.name : '';

			_prclog('Process', 'start to get track url', json);

			// patch resource 
			return lastfm.resource(artist, track)
				.then(source_res => {

					// 取不到資源，只能將歌曲加入 hatelist
					if (!source_res.result) {
						_prclog('Fail', 'can not get track url, so add to hate list', source_res);
						ai_core.hate_list[mbid] = true;
						return source_res;
					}
					else {
						_prclog('Success', 'track url success', source_res);
						json.data.resource = source_res.data;
						return json;
					}
				});
		})
		.catch(e => ({ result: false, error: e, data: {} }));
};

// 歌曲細節 (字串查詢)
lastfm.tracks_string = function (artist, track, track_mbid) {
	_apilog(`${lastfm_url}?method=track.getInfo&track=${track}&artist=${artist}&api_key=${apikey}&format=json`);
	return $.getJSON(`${lastfm_url}?method=track.getInfo&track=${track}&artist=${artist}&api_key=${apikey}&format=json`)
		.then(json => {
			//console.log('[use string] track.getInfo', json);
			if (json.track) {
				json.track.mbid = track_mbid;
				json.track.nombid = true;
				_prclog('Success', 'get track detail success', json);
				return { result: true, data: json.track };
			}
			_prclog('Fail', 'can not get track detail', json);
			return { result: false, error: '[use string] can not get track detail', data: {} };
		})
		.then(json => {
			if (!json.result) return json;
			var track = json.data.name || '';
			var artist = json.data.artist ? json.data.artist.name : '';
			// patch resource 

			_prclog('Process', 'start to get track url', json);

			return lastfm.resource(artist, track)
				.then(source_res => {
					//console.log('[use string] lastfm.resource', source_res);
					// 取不到資源，只能將歌曲加入 hatelist
					if (!source_res.result) {
						_prclog('Fail', 'can not get track url, so add to hate list', source_res);
						if (json.data.mbid) ai_core.hate_list[json.data.mbid] = true;
						return source_res;
					}
					else {
						_prclog('Success', 'track url success', source_res);
						json.data.resource = source_res.data;
						return json;
					}
				});
		})
		.catch(e => ({ result: false, error: e, data: {} }));
};

// 搜尋音樂資源
lastfm.resource = function (artist, track) {
	_apilog(`/music/search?keywords=${artist} ${track}`);
	return $.getJSON(`/music/search?keywords=${artist} ${track}`)
		.then(function (data) {
			// 沒有資源可以下載
			if (!data.result || !data.result.songs || !data.result.songs.length) {
				_prclog('Fail', 'can not find track', data);
				return { result: false, error: 'can not get source because NetEase no music', data: null };
			}

			var list = data.result.songs;
			var source = null;
			var score = 0;
			// 擂台法找出分數最高 (所以不一定對)
			var log_pool = [];
			var winer_idx = 0;
			for (var i = 0; i < list.length; i++) {
				let song = list[i];
				let a = renderProfile(song.artists || song.ar);
				let song_name = song.name;
				let artist_name = a.name;
				let new_score = ai_core.analysis_similarity(artist, track, artist_name, song_name);
				log_pool.push({ score: new_score, artist: [artist, artist_name], track: [track, song_name] });
				if (new_score > score) {
					winer_idx = i;
					score = new_score;
					source = "/music/download?id=" + song.id + "&filename=" + artist_name + " - " + song_name;
				}
			}

			_prclog('Process', 'who is the slimiartyest', log_pool);
			_prclog('Process', 'score=' + score + ' winer is', list[winer_idx]);
			//console.log('analysis_similarity', log_pool);
			//console.log('analysis_similarity winer score= ' + score + ' is', list[winer_idx]);
			if (score < 0.7) {
				_prclog('Process', 'score=' + score + ' is too low, so reject the source', list[winer_idx]);
				//console.log('analysis_similarity score too low, so reject the source');
				source = null;
			}

			return source ? { result: true, data: source } : { result: false, error: 'can not get source', data: null };
		})
		.catch(e => ({ result: false, data: null, error: e }));
};


// 歌曲的相似歌曲
lastfm.similarity_track = function (mbid) {
	_apilog(`${lastfm_url}?method=track.getsimilar&mbid=${mbid}&api_key=${apikey}&limit=20&format=json`);
	return $.getJSON(`${lastfm_url}?method=track.getsimilar&mbid=${mbid}&api_key=${apikey}&limit=20&format=json`)
		.then(json => {
			//console.log('find similarity track', ai_core.like_list[mbid], json);
			var list = json.similartracks.track;
			if (list.length < 1) {
				_prclog('Fail', 'can not find similarity tracks list', json);
				return { result: false, data: [], error: 'can not get similarity track' };
			}
			_prclog('Success', 'find similarity tracks list success', list);
			_prclog('Process', 'start to get one top track of list', list);
			// 最佳歌曲
			return ai_core.findTopTrack(list).then(json => get_first_tracks(json.data));
		})
		.catch(e => ({ result: false, data: null, error: e }))
};

// 歌曲的相似歌曲 (字串查詢)
lastfm.similarity_track_string = function (artist, track, mbid) {
	//var artist = ai_core.like_list[mbid].artist;
	//var track = ai_core.like_list[mbid].track;
	_apilog(`${lastfm_url}?method=track.getsimilar&track=${track}&artist=${artist}&api_key=${apikey}&autocorrect=1&limit=20&format=json`);
	return $.getJSON(`${lastfm_url}?method=track.getsimilar&track=${track}&artist=${artist}&api_key=${apikey}&autocorrect=1&limit=20&format=json`)
		.then(json => {
			//console.log('[use string] find similarity track', { artist, track, mbid }, json);
			var list = json.similartracks.track;
			if (list.length < 1) {
				_prclog('Fail', 'can not find similarity tracks list', json);
				return { result: false, data: [], error: 'can not get similarity track' };
			}
			_prclog('Process', 'success find similarity tracks list', list);
			_prclog('Process', 'so start to get one top track of list ↑');
			// 最佳歌曲
			return ai_core.findTopTrack(list).then(json => get_first_tracks(json.data));
		})
		.catch(e => ({ result: false, data: null, error: e }));
};

// 歌手的相似歌手 (字串查詢)
lastfm.similarity_artist = function (artist) {
	_apilog(`${lastfm_url}?method=artist.getsimilar&artist=${artist}&api_key=${apikey}&limit=6&format=json`);
	return $.getJSON(`${lastfm_url}?method=artist.getsimilar&artist=${artist}&api_key=${apikey}&limit=6&format=json`)
		.then(json => {
			var list = json.similarartists.artist;
			if (!list || list.length < 1) {
				_prclog('Fail', 'can not find similarity artist list', json);
				return { result: false, data: [], error: 'can not get similarity artist' };
			}
			_prclog('Process', 'success find similarity artist list', list);
			return { result: true, data: list };
		})
		.catch(e => ({ result: false, data: null, error: e }));
};


// 依照順序找歌曲資源是否可用，如過第一首可用將直接回傳不會再取後面的
function get_first_tracks(list) {
	if (!list || list.length < 1) {
		return { result: false, error: 'can not get top getsimilar track', data: {} };
	}
	var i = 0, top_track = list[i];
	var p = top_track.nombid ? lastfm.tracks_string(top_track.artist.name, top_track.name, top_track.mbid)
		: lastfm.tracks(top_track.mbid);
	for (i = 1; i < list.length; i++) {
		p = (top_track => p.then(json => {
			return json.result ? json : top_track.nombid ? lastfm.tracks_string(top_track.artist.name, top_track.name, top_track.mbid)
				: lastfm.tracks(top_track.mbid);
		}))(list[i]);
	}
	return p;
}


// 找出 Track mbid

lastfm.get_track_mbid = function (artist, track) {
	_apilog(`https://musicbrainz.org/ws/2/release?query=release:${track} AND artist:${artist}&fmt=json`);
	return $.getJSON(`https://musicbrainz.org/ws/2/release?query=release:${track} AND artist:${artist}&fmt=json`)
		.then(json => {
			//console.log('find track mbid', json);
			if (json.releases.length < 1) {
				_prclog('Fail', 'can not get track mbid', json);
				return { result: false, data: {}, error: 'can not get track mbid' };
			}
			_prclog('Success', 'get track mbid =' + json.releases[0].id);
			return { result: true, data: json.releases[0] };
		})
		.catch(e => ({ result: false, data: null, error: e }));
};


// 折衷方案 youtube
lastfm.youtube = function (q) {
	_apilog(`/music/youtube?q=${q}`);
	return $.getJSON(`/music/youtube?q=${q}`)
		.then(json => {
			if (json.url) {
				_prclog('Success', 'get youtube url', json);
				return { result: true, data: json.url };
			}
			_prclog('Fail', 'can not get youtube url', json);
			return { result: false, data: null };
		})
		.catch(e => ({ result: false, data: null, error: e }));
};



// 折衷方案2 VK
lastfm.vk = function (q) {
	_apilog(`/music/vk?q=${q}`);
	return $.getJSON(`/music/vk?q=${q}`)
		.then(json => {
			if (json.url) {
				_prclog('Success', 'get vk url', json);
				return Object.assign({ result: true }, json);
			}
			_prclog('Fail', 'can not get vk url', json);
			return { result: false };
		})
		.catch(e => {
			_prclog('Fail', 'can not get vk url', e);
			return { result: false, error: e };
		});
};