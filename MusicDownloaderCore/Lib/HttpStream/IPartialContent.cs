﻿using System.Collections.Generic;
using System.IO;

namespace MusicDownloaderCore.Lib.HttpStream
{
    /// <summary>
    /// "部分內容"功能必須實作的規格介面
    /// https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Range_requests
    /// </summary>
    public interface IPartialContent
    {
        /// <summary>
        /// 檔案開始 byte 位置 
        /// </summary>
        long StartPoint { get; }

        /// <summary>
        /// 檔案結束 byte 位置 
        /// </summary>
        long EndPoint { get; }

        /// <summary>
        /// 串流記憶體緩衝大小，每次讀取檔案的緩存
        /// </summary>
        long BufferLength { get; set; }

        /// <summary>
        /// 取得檔案資訊
        /// </summary>
        /// <returns></returns>
        FileInfo FileInfo { get; set; }


        /// <summary>
        /// 迭代器：使用檔案讀取串流將檔案讀進緩衝區回傳，直到整份檔案被讀完才會結束迭代
        /// </summary>
        /// <param name="fs">檔案讀取串流</param>
        /// <returns>會傳 Tuple 一整個緩衝區的資料與讀取長度</returns>
        IEnumerable<(byte[] buffer, int bytesRead)> GetPartialContent(Stream fs);

    }
}
