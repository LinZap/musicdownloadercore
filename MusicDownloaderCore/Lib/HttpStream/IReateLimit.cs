﻿namespace MusicDownloaderCore.Lib.HttpStream
{
    /// <summary>
    /// "串流限速"功能必須實作的規格介面
    /// </summary>
    public interface IReateLimit
    {
        /// <summary>
        /// 基於程式啟動到開始進行串流工作經過的豪秒數
        /// </summary>
        long ProcessStartMs { get; }

        /// <summary>
        /// 每秒允許的傳輸 btye 大小
        /// </summary>
        long MaximumBytesPerSecond { get; set; }

        /// <summary>
        /// 總傳輸 byte 大小 (用於計算整體傳輸速率)
        /// </summary>
        long BufferByteCount { get; }

        /// <summary>
        /// 計算需要延遲多少毫秒，才能讀下一輪的 buffer
        /// </summary>
        /// <param name="CurrentBufferByte">這一輪輸出的 buffer 大小</param>
        /// <returns>需要等待幾秒，毫秒部分無條件捨去</returns>
        int GetDalayMs(int CurrentBufferByte);
    }

}
