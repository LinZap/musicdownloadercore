﻿using System;
using System.Net;

namespace MusicDownloaderCore.Lib
{
    public class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest WR = base.GetWebRequest(uri);
            WR.Timeout = 23000;
            return WR;
        }
    }
}