﻿using MusicDownloaderCore.Models.MusicAPI;
using MusicDownloaderCore.Models.VK;
using MusicDownloaderCore.Models.Youtube;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using ZapLib;
using ZapLib.Utility;

namespace MusicDownloaderCore.Lib
{
    public class MusicAPI
    {
        private string host;
        private MyLog log;
        public MusicAPI()
        {
            log = new MyLog();
            host = Config.Get("NetEase") ?? "http://localhost:3000";

        }

        private void apilog(string url, object data)
        {
            log.Write("Host: " + url);
            log.Write("Data: " + JsonConvert.SerializeObject(data));
        }

        /// <summary>
        /// 搜尋音樂，返回30首結果
        /// </summary>
        /// <param name="keywords">關鍵字</param>
        /// <returns>搜尋結果列表</returns>
        public ModelSearch Search(string keywords)
        {
            apilog(host + "/search", new { keywords/*, proxy = Config.Get("Proxy")*/ });
            Fetch f = new Fetch(host + "/search");
            try
            {
                return f.Get<ModelSearch>(new { keywords/*, proxy = Config.Get("Proxy")*/ });
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        ///搜尋建議 /search/suggest?keywords=
        /// </summary>
        /// <param name="keywords">關鍵字</param>
        /// <returns>建議結果列表</returns>
        public ModelSuggest SearchSuggest(string keywords)
        {
            apilog(host + "/search/suggest", new { keywords, type = "mobile", proxy = Config.Get("Proxy") });
            Fetch f = new Fetch(host + "/search/suggest");
            try
            {
                return f.Get<ModelSuggest>(new { keywords, type = "mobile", proxy = Config.Get("Proxy") });
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 取得音樂下載資源
        /// </summary>
        /// <param name="music_id"></param>
        /// <returns></returns>
        public ModelSongURL GetResource(int music_id)
        {
            apilog(host + "/song/url", new { id = music_id, /*proxy = Config.Get("Proxy")*/ });
            Fetch f = new Fetch(host + "/song/url");
            try
            {
                return f.Get<ModelSongURL>(new { id = music_id, /*proxy = Config.Get("Proxy")*/ });
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 取得音樂詳細內容
        /// </summary>
        /// <param name="music_id"></param>
        /// <returns></returns>
        public ModelSong2 GetSongDetail(int music_id)
        {
            apilog(host + "/song/detail", new { ids = music_id });
            Fetch f = new Fetch(host + "/song/detail");
            try
            {
                return f.Get<ModelSong2>(new { ids = music_id });
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 取得播放清單音樂列表
        /// </summary>
        /// <param name="playlist_id"></param>
        /// <returns></returns>
        public ModelPlayListDetail GetPlayList(string playlist_id)
        {
            apilog(host + "/playlist/detail", new { id = playlist_id });
            Fetch f = new Fetch(host + "/playlist/detail");
            try
            {
                return f.Get<ModelPlayListDetail>(new { id = playlist_id });
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 代理伺服器 VPN 下載 (請設定 Web.config 中的 Proxy)
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public byte[] AgentDownload(string url)
        {
            apilog(url, null);
            NewFetch f = new NewFetch(url);

            f.SetTimeout(25);
            //f.Proxy = Config.Get("Proxy");
            string err = null;
            byte[] res = null;
            try
            {
                res = f.GetBinary(null);
            }
            catch (Exception e)
            {
                err = e.ToString();
            }

            if (res == null)
            {
                MyLog log = new MyLog();
                log.Write($"[{f.StatusCode}] {url}");
                if (err != null) log.Write(err);
            }
            return res;
        }

        /// <summary>
        /// 下載 youtube 影片
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public byte[] YoutubeDownload(string url)
        {
            byte[] file = null;
            apilog(url, null);
            try
            {
                using (MyWebClient client = new MyWebClient())
                {
                    file = client.DownloadData(url);
                }

                if (file == null) apilog("無法下載檔案", null);
            }
            catch (Exception e)
            {
                apilog(url, new { error = e.ToString() });
            }

            return file;
        }


        public ModelYtdl YouTubeDownloadURL(string q)
        {
            //api?format=1&artist=中川翔子&track=God Knows...
            apilog("http://localhost:8888/api", new { q });
            Fetch f = new Fetch("http://localhost:8888/api");
            try
            {
                return f.Get<ModelYtdl>(new { q });
            }
            catch
            {
                return null;
            }
        }

        public ModelVK VKDownloadURL(string q)
        {
            apilog("http://localhost:8888/vk", new { q });
            Fetch f = new Fetch("http://localhost:8888/vk");
            try
            {
                return f.Get<ModelVK>(new { q });
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 取得單一專輯訊息
        /// </summary>
        /// <param name="id">專輯 ID</param>
        /// <returns>專輯資訊</returns>
        public ModelAlbum GetSingleAlbum(int id)
        {
            apilog(host + "/album", new { id });
            Fetch f = new Fetch(host + "/album");

            try
            {
                return f.Get<ModelAlbum>(new { id });
            }
            catch
            {
                return null;
            }

        }


        /// <summary>
        /// 取得歌手專輯列表
        /// </summary>
        /// <param name="id">歌手 ID</param>
        /// <returns>歌手專輯資訊</returns>
        public ModelArtistAlbum GetArtistAlbum(int id)
        {
            apilog(host + "/artist/album", new { id });
            Fetch f = new Fetch(host + "/artist/album");
            try
            {
                return f.Get<ModelArtistAlbum>(new { id });
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 確認歌曲是否可用
        /// </summary>
        /// <param name="id">歌曲 ID</param>
        /// <returns></returns>
        public ModelCheckMusic CheckMusic(int id)
        {
            apilog(host + "/check/music", new { id });
            Fetch f = new Fetch(host + "/check/music");
            try
            {
                return f.Get<ModelCheckMusic>(new { id }) ?? new ModelCheckMusic() { success = false };
            }
            catch
            {
                return new ModelCheckMusic() { success = false };
            }
        }




        public ModelLatestSongs GetLatestSongs(int type = 8)
        {
            apilog(host + "/top/song", new { type });
            Fetch f = new Fetch(host + "/top/song");
            try
            {
                return f.Get<ModelLatestSongs>(new { type });
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
                return null;
            }
        }


    }
}