﻿using MusicDownloaderCore.Models.MusicDownloader;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using ZapLib.Utility;

namespace MusicDownloaderCore.Lib
{
    public class I3SFileSystem
    {
        private string rootPath;
        private int mid = 0;
        private string youtube_id = null;

        public I3SFileSystem(string rootPath = null)
        {
            this.rootPath = string.IsNullOrWhiteSpace(rootPath) ? Config.Get("Root") : rootPath;
            if (!Directory.Exists(this.rootPath)) throw new Exception("Can not get File System Root Directory");
        }

        /// <summary>
        /// 網易音樂註冊
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="rootPath"></param>
        public I3SFileSystem(int mid, string rootPath = null)
        {
            this.rootPath = string.IsNullOrWhiteSpace(rootPath) ? Config.Get("Root") : rootPath;
            if (!Directory.Exists(this.rootPath)) throw new Exception("Can not get File System Root Directory");
            this.mid = mid;
        }

        /// <summary>
        /// YOUTUBE 註冊
        /// </summary>
        /// <param name="mid">必為 0</param>
        /// <param name="youtube_id">YOUTUBE ID</param>
        public I3SFileSystem(int mid, string youtube_id, string rootPath = null)
        {
            this.rootPath = string.IsNullOrWhiteSpace(rootPath) ? Config.Get("Root") : rootPath;
            if (!Directory.Exists(this.rootPath)) throw new Exception("Can not get File System Root Directory");
            this.mid = mid;
            this.youtube_id = youtube_id;
        }




        public string convertIdToPath(long oid)
        {
            string HexPath = Convert.ToString(oid, 16);
            while (HexPath.Length < 8) HexPath = "0" + HexPath;
            HexPath = HexPath.Insert(2, "\\");
            HexPath = HexPath.Insert(5, "\\");
            HexPath = HexPath.Insert(8, "\\");
            checkDirExist(HexPath);
            return rootPath + "\\" + HexPath;
        }

        private void checkDirExist(string path)
        {
            string[] sp = path.Split('\\');
            string treePath = rootPath;
            if (!Directory.Exists(treePath)) Directory.CreateDirectory(treePath);
            for (int i = 0; i < sp.Length; i++)
            {
                if (!Directory.Exists(treePath)) Directory.CreateDirectory(treePath);
                treePath += "\\" + sp[i];
            }
        }


        public ModelRegisterFile register(string filename = null)
        {
            SQLite db = new SQLite();
            string uuid = Guid.NewGuid().ToString();
            if (string.IsNullOrWhiteSpace(filename)) filename = uuid;

            string mimeType = MimeMapping.GetMimeMapping(filename);
            var res = db.quickQuery("insert into Object(uuid,cname,ename,mid)values(@uuid,@mimeType,@ename,@mid); SELECT last_insert_rowid() as oid", new { uuid, mimeType, ename = filename, mid });
            if (res == null) return null;
            try
            {
                if (res.Count() < 1) return null;
                long oid = res.Single().oid;
                string dist = convertIdToPath(oid);
                return new ModelRegisterFile()
                {
                    oid = oid,
                    dist = dist,
                    uuid = uuid,
                    mimeType = mimeType,
                    filename = filename
                };
            } catch (Exception e)
            {
                Trace.WriteLine("register filename: " + filename);
                Trace.WriteLine(e.ToString());
                return null;
            }

        }

        public ModelRegisterFile register_youtube(string youtube_id, string extension, string mimeType)
        {
            SQLite db = new SQLite();
            string uuid = Guid.NewGuid().ToString();
            string filename = uuid + "." + extension;
            int mid = 0;
            var res = db.quickQuery("insert into Object(uuid,cname,ename,mid)values(@uuid,@mimeType,@ename,@mid); SELECT last_insert_rowid() as oid",
                new { uuid = youtube_id, mimeType, ename = filename, mid });
            if (res == null) return null;
            try
            {
                if (res.Count() < 1) return null;
                long oid = res.Single().oid;
                string dist = convertIdToPath(oid);
                return new ModelRegisterFile()
                {
                    oid = oid,
                    dist = dist,
                    uuid = youtube_id,
                    mimeType = mimeType,
                    filename = filename
                };
            }catch(Exception e)
            {
                Trace.WriteLine("register_youtube: " + youtube_id);
                Trace.WriteLine(e.ToString());
                return null;
            }
        }

        public ModelRegisterFile getRegisterByYouTubeId(string youtube_id)
        {
            SQLite db = new SQLite();
            var res = db.quickQuery("select * from Object where mid=@mid and uuid=@uuid limit 1", new { mid = 0, uuid = youtube_id });
            if (res == null) return null;
            try
            {
                if (res.Count() < 1) return null;
                dynamic row = res.Single();
                string filename = row.ename;
                if (string.IsNullOrWhiteSpace(filename)) filename = row.uuid;
                return new ModelRegisterFile()
                {
                    oid = row.oid,
                    dist = convertIdToPath(row.oid),
                    uuid = row.uuid,
                    mimeType = row.cname,
                    filename = filename,
                    mid = row.mid,
                };
            }
            catch (Exception e)
            {
                Trace.WriteLine("getRegisterByYouTubeId mid: " + mid);
                Trace.WriteLine(e.ToString());
                return null;
            }
        }

        public ModelRegisterFile getRegisterByMid()
        {
            SQLite db = new SQLite();

            var res = db.quickQuery("select * from Object where mid=@mid limit 1", new { mid });
            if (res == null) return null;
            try
            {
                if (res.Count() < 1) return null;
                dynamic row = res.Single();
                string filename = row.ename;
                if (string.IsNullOrWhiteSpace(filename)) filename = row.uuid;
                return new ModelRegisterFile()
                {
                    oid = row.oid,
                    dist = convertIdToPath(row.oid),
                    uuid = row.uuid,
                    mimeType = row.cname,
                    filename = filename,
                    mid = row.mid,
                };
            }
            catch (Exception e)
            {
                Trace.WriteLine("mid getRegisterByMid: " + mid);
                Trace.WriteLine(e.ToString());
                return null;
            }
        }

        public ModelRegisterFile getRegisterFile(string uuid)
        {
            SQLite db = new SQLite();
            var res = db.quickQuery("select * from Object where uuid=@uuid limit 1", new { uuid });
            if (res == null) return null;
            try
            {
                if (res.Count() < 1) return null;
                dynamic row = res.Single();
                string filename = row.ename;
                if (string.IsNullOrWhiteSpace(filename)) filename = uuid;
                return new ModelRegisterFile()
                {
                    oid = row.oid,
                    dist = convertIdToPath(row.oid),
                    uuid = row.uuid,
                    mimeType = row.cname,
                    filename = filename,
                    mid = row.mid,
                };
            }catch(Exception e)
            {
                Trace.WriteLine("mid getRegisterFile: " + uuid);
                Trace.WriteLine(e.ToString());
                return null;
            }
        }


    }
}