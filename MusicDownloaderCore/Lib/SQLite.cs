﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using ZapLib;
using ZapLib.Utility;

namespace MusicDownloaderCore.Lib
{
    public class SQLite
    {
        private string dbPath;
        private string connStr;
        Lazy<MyLog> mylog = new Lazy<MyLog>();

        public SQLite(string dbPath = null, string dbName = "db.sqlite")
        {
            this.dbPath = string.IsNullOrWhiteSpace(dbPath) ? Config.Get("Root") : dbPath;
            if (string.IsNullOrWhiteSpace(this.dbPath)) throw new Exception("Can not get File System Root Directory");
            this.dbPath += @"\" + dbName;
            connStr = "data source=" + this.dbPath;
            if (!initDB()) throw new Exception("Can not Init DB");
            patch("20181115patch", "alter table Object add COLUMN ename char(260) null;");
            patch("20190316patch", "alter table Object add COLUMN mid INTEGER not null default (0); CREATE UNIQUE INDEX uq_mid on Object (mid);");
            patch("20200423patch", "drop INDEX uq_mid");
        }

        private bool initDB()
        {
            if (File.Exists(dbPath)) return true;
            string initSQLPath = Config.Get("Root") + @"\init.sql";
            if (!File.Exists(initSQLPath)) return false;
            string intsql = File.ReadAllText(initSQLPath);
            return quickExec(intsql);
        }

        private void patch(string pathname, string sql)
        {
            string isPatch = Config.Get("Root") + @"\" + pathname;
            if (!File.Exists(isPatch))
            {
                File.WriteAllText(isPatch, "");
                quickExec(sql);
            }
        }

        public IEnumerable<dynamic> quickQuery(string sql, object param = null, bool isfetchAll = true)
        {
            using (var cn = new SQLiteConnection(connStr))
            {
                try
                {
                    return cn.Query(sql, param);
                }
                catch (Exception e)
                {

                    log("SQL: " + sql);
                    log(e.ToString());
                    return null;
                }
            }
        }

        public bool quickExec(string sql, object param = null)
        {
            using (var cn = new SQLiteConnection(connStr))
            {
                try
                {
                    cn.Execute(sql, param);
                    return true;
                }
                catch (Exception e)
                {
                    log("SQL: " + sql);
                    log(e.ToString());
                    return false;
                }
            }
        }

        private void log(string msg)
        {
            mylog.Value.Write(msg);
        }
    }
}