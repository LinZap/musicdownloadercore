﻿using System;
using System.Net;
using System.Net.Http;
using ZapLib;
using ZapLib.Utility;

namespace MusicDownloaderCore.Lib
{
    public class NewFetch : Fetch
    {
        private WebProxy WebProxy;
        private CookieContainer CookieContainer;

        public new string Proxy
        {
            set
            {
                WebProxy.Address = value == null ? null : new Uri(value);
                WebProxy.BypassProxyOnLocal = value != null;
            }
            get => WebProxy?.Address.ToString();
        }
        /// <summary>
        /// HTTP Cookie
        /// </summary>
        public new object Cookie
        {
            set
            {
                Mirror.EachMembers(value, (string key, string val) =>
                {
                    if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(val)) return;
                    CookieContainer.Add(new Cookie(key, val) { Domain = Client.BaseAddress.Host });
                });
            }
            get => Client.BaseAddress == null ? null : CookieContainer.GetCookies(Client.BaseAddress);
        }


        /// <summary>
        /// 建構子，初始化必要物件
        /// </summary>
        /// </summary>
        /// <param name="uri"></param>
        public NewFetch(string uri = null)
        {
            CookieContainer = new CookieContainer();
            WebProxy = new WebProxy();
            Client = new HttpClient(new HttpClientHandler() { CookieContainer = CookieContainer, Proxy = WebProxy });
            Request = new HttpRequestMessage();
            Url = uri;
            
        }

        /// <summary>
        /// 設定最多存取時間
        /// </summary>
        /// <param name="sec"></param>
        public void SetTimeout(int sec)
        {
            Client.Timeout = TimeSpan.FromSeconds(sec);
        }
    }
}