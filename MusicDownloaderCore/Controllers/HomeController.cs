﻿using MusicDownloaderCore.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicDownloaderCore.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.DEBUG = new ModeDetector().IsDebug? "true": "false";
            ViewBag.ISAIPLAYER = "false";
            return View();
        }
    }
}
