﻿using MusicDownloaderCore.Lib;
using MusicDownloaderCore.Lib.HttpStream;
using MusicDownloaderCore.Models.MusicAPI;
using MusicDownloaderCore.Models.MusicDownloader;
using MusicDownloaderCore.Models.VK;
using MusicDownloaderCore.Models.Youtube;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using ZapLib;

namespace MusicDownloaderCore.Controllers
{
    [RoutePrefix("music")]
    public class MusicController : ApiController
    {
        [HttpGet]
        [Route("search")]
        public HttpResponseMessage search(string keywords)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            var result = m.Search(keywords);
            if (result == null) return api.GetResponse(new { error = "can not search" }, HttpStatusCode.Forbidden);
            return api.GetResponse(result);
        }
        /// <summary>
        /// 搜尋建議 ["...","...",...]
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("suggest")]
        public HttpResponseMessage suggest(string keywords)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            var result = m.SearchSuggest(keywords);
            if (result == null) return api.GetResponse(new { error = "can not get suggest" }, HttpStatusCode.Forbidden);
            string[] suggests = result.result.allMatch.Select(element => element.keyword).ToArray();
            return api.GetResponse(suggests);
        }

        [HttpGet]
        [Route("album")]
        public HttpResponseMessage album(int id)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            var result = m.GetSingleAlbum(id);
            if (result == null) return api.GetResponse(new { error = "can not get single album" }, HttpStatusCode.Forbidden);
            return api.GetResponse(result);
        }

        [HttpGet]
        [Route("artist")]
        public HttpResponseMessage artist(int id)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            var result = m.GetArtistAlbum(id);
            if (result == null) return api.GetResponse(new { error = "can not get artist album" }, HttpStatusCode.Forbidden);
            return api.GetResponse(result);
        }

        [HttpGet]
        [Route("latestsongs")]
        public HttpResponseMessage latestSongs(int type = 8)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            var result = m.GetLatestSongs(type);
            if (result == null) return api.GetResponse(new { error = "can not get artist album" }, HttpStatusCode.Forbidden);
            return api.GetResponse(result.data);
        }

        [HttpGet]
        [Route("detail")]
        public HttpResponseMessage detail(int id)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            Lib.MusicAPI m = new MusicAPI();
            var data = m.GetSongDetail(id);
            if (data == null || data.songs.Count < 1) return api.GetTextResponse("無法取得音樂細節", HttpStatusCode.Forbidden);
            var detail = data.songs[0];
            return api.GetResponse(new ModelMusic
            {
                id = detail.id,
                author = detail.ar.First().name,
                authorUrl = "#",
                fileName = detail.name,
                fileUrl = "/music/download?id=" + detail.id + "&filename=" + detail.ar.First().name + " - " + detail.name,
                thumb = detail.al.picUrl
            });

        }

        [HttpGet]
        [Route("playlist")]
        public HttpResponseMessage playlist(string id)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            var data = m.GetPlayList(id);
            if (data == null || data.playlist.tracks.Count < 1) return api.GetTextResponse("無法取得播放清單", HttpStatusCode.Forbidden);

            List<ModelMusic> result = new List<ModelMusic>();
            foreach (ModelSong2Detail detail in data.playlist.tracks)
            {
                result.Add(new ModelMusic()
                {
                    id = detail.id,
                    author = detail.ar.First().name,
                    authorUrl = "#",
                    fileName = detail.name,
                    fileUrl = "/music/download?id=" + detail.id + "&filename=" + detail.ar.First().name + " - " + detail.name,
                    thumb = detail.al.picUrl
                });
            }
            return api.GetResponse(result);
        }

        [HttpGet]
        //[CacheOutput(ClientTimeSpan = 300, ServerTimeSpan = 300)]
        [Route("youtube")]
        public HttpResponseMessage getYoutubeURL(string q)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            ModelYtdl ytdl = m.YouTubeDownloadURL(q);
            if (ytdl == null || string.IsNullOrWhiteSpace(ytdl.url))
                return api.GetTextResponse("連 Youtube 的連結都沒有!", HttpStatusCode.Forbidden);
            return api.GetResponse(ytdl);
        }


        [HttpGet]
        [Route("vk")]
        public HttpResponseMessage getVKURL(string q)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            MusicAPI m = new MusicAPI();
            ModelVK vk = m.VKDownloadURL(q);
            if (vk == null || string.IsNullOrWhiteSpace(vk.url))
                return api.GetTextResponse("不能取得 VK URL", HttpStatusCode.Forbidden);
            return api.GetResponse(vk);
        }

        [HttpGet]
        //[CacheOutput(ClientTimeSpan = 300, ServerTimeSpan = 300)]
        [Route("download")]
        public HttpResponseMessage downloadLink(int id, string filename)
        {
            ExtApiHelper api = new ExtApiHelper(this);

            try
            {
                HttpContext.Current.Server.ScriptTimeout = 23;
                MusicAPI m = new MusicAPI();
                //I3SFileSystem ytfs = new I3SFileSystem();
                I3SFileSystem fs = new I3SFileSystem(id);

                // 去除不合法名稱
                foreach (char c in Path.GetInvalidPathChars())
                {
                    filename = filename.Replace(c, ' ');
                }

                string q = Path.GetFileName(filename);
                //ModelYtdl ytdl = m.YouTubeDownloadURL(q);
                ModelRegisterFile file = fs.getRegisterByMid();

                //if (file == null && ytdl != null) file = ytfs.getRegisterByYouTubeId(ytdl.id);
                //if (file != null) return getYoutubeRegisterFile(api, ytdl, q);

                if (file == null)
                {
                    string dowlonad_link = null;

                    var check_music = m.CheckMusic(id);

                    if (!check_music.success) return api.GetTextResponse("該資源無法取得，因為版權因素", HttpStatusCode.Forbidden);
                    else
                    {
                        var result = m.GetResource(id);
                        try
                        {
                            if (string.IsNullOrWhiteSpace(result.data[0].url)) return api.GetTextResponse("該資源無法取得，下載的 URL 為空", HttpStatusCode.Forbidden);
                            else dowlonad_link = result.data[0].url;
                        }
                        catch (Exception)
                        {
                            return api.GetTextResponse("該資源無法取得，因為無法正確取得 URL", HttpStatusCode.Forbidden);
                        }
                    }

                    byte[] music_resource = m.AgentDownload(dowlonad_link);
                    if (music_resource == null) return api.GetTextResponse("該資源無法取得，因為無法透過代理下載音樂", HttpStatusCode.Forbidden);

                    string fn = Path.GetFileName(dowlonad_link);
                    file = fs.register(fn);

                    if (file == null) return api.GetTextResponse("該資源無法取得，因為I3S檔案系統無法註冊這個檔案", HttpStatusCode.Forbidden);

                    // save to hdd
                    File.WriteAllBytes(file.dist, music_resource);
                    music_resource = null;
                }

                string[] sp = file.filename.Split('.');
                if (sp.Length > 1) filename = filename + "." + sp[sp.Length - 1];

                (long StartPoint, long? EndPoint, bool IsPartial) = GetHeaderRange();
                StreamResponse sr = new StreamResponse(file.dist, StartPoint, EndPoint);
                sr.FileName = filename;
                sr.IsPartial = IsPartial;
                sr.StartProcessing();
                return sr.Response;
            }
            catch (Exception e)
            {
                return api.GetTextResponse("發生嚴重錯誤: " + e, HttpStatusCode.Forbidden);
            }

        }

        /// <summary>
        /// 改由 youtuebe 輸出
        /// </summary>
        /// <param name="api">api 物件</param>
        /// <param name="q">查詢字串(去除附檔名)</param>
        /// <returns></returns>
        private HttpResponseMessage getYoutubeRegisterFile(ExtApiHelper api, ModelYtdl ytdl, string q)
        {
            MusicAPI m = new MusicAPI();
            if (ytdl == null || string.IsNullOrWhiteSpace(ytdl.url)) return api.GetTextResponse("該資源無法取得，因為無法取得 YOUTUBE 聲音 URL", HttpStatusCode.Forbidden);
            I3SFileSystem ytfs = new I3SFileSystem();
            ModelRegisterFile file = ytfs.getRegisterByYouTubeId(ytdl.id);

            if (file == null)
            {
                byte[] music_resource = m.YoutubeDownload(ytdl.url);
                if (music_resource == null) return api.GetTextResponse("該資源無法取得，因為無法 YOUTUBE 下載該音樂檔案", HttpStatusCode.Forbidden);
                file = ytfs.register_youtube(ytdl.id, ytdl.extension, ytdl.mimeType);
                // save to hdd
                File.WriteAllBytes(file.dist, music_resource);
                music_resource = null;
            }

            (long StartPoint, long? EndPoint, bool IsPartial) = GetHeaderRange();
            StreamResponse sr = new StreamResponse(file.dist, StartPoint, EndPoint);
            sr.FileName = q + "." + ytdl.extension;
            sr.IsPartial = IsPartial;
            sr.StartProcessing();

            Trace.WriteLine(JsonConvert.SerializeObject(file));
            return sr.Response;
        }


        /// <summary>
        /// 取得 Header 中的 Range
        /// </summary>
        /// <returns></returns>
        [NonAction]
        private (long StartPoint, long? EndPoint, bool IsPartial) GetHeaderRange()
        {
            RangeHeaderValue rangeHeader = Request.Headers.Range;
            // 只有 Range 不是 NULL 且單位為 bytes 且只有一組時，才是 partial content
            if (rangeHeader != null && rangeHeader.Unit == "bytes" && rangeHeader.Ranges.Count == 1)
            {
                RangeItemHeaderValue range = rangeHeader.Ranges.OfType<RangeItemHeaderValue>().FirstOrDefault();
                return (range.From ?? 0, range.To, true);
            }
            // 其餘設定均視為下載全部檔案
            else
                return (0, null, false);
        }


    }
}
