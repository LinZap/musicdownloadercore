﻿using MusicDownloaderCore.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicDownloaderCore.Controllers
{
    public class AiPlayerController : Controller
    {
        // GET: AiPage
        public ActionResult Index()
        {
            ViewBag.DEBUG = new ModeDetector().IsDebug ? "true" : "false";
            ViewBag.ISAIPLAYER = "true";
            return View();
        }
    }
}